#!/bin/bash

if [ -z $1 ] ;
then echo "Using: ./docker_shell_jump.sh [web|postgres]";
     exit 0;
fi

CONTAINER_ID=$(docker ps | grep $1 | awk '{print $1}')
docker exec -ti $CONTAINER_ID bash
