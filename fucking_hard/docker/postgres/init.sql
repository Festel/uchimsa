-- Modify template database
-- pgcrypto allow to use gen_random_uuid()
\c template1;
CREATE EXTENSION pgcrypto;
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION postgis_sfcgal;
CREATE EXTENSION fuzzystrmatch;
CREATE EXTENSION address_standardizer;
CREATE EXTENSION address_standardizer_data_us;
CREATE EXTENSION postgis_tiger_geocoder;
CREATE DATABASE uchimsa;
CREATE USER alo with superuser password '123';
GRANT ALL privileges ON DATABASE uchimsa TO alo;
