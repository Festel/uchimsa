from flask import Blueprint, render_template, url_for
from src.db import db
from src.db.models import AtFirst

mod = Blueprint('home', __name__)

@mod.route("/")
def hello():
    print("------------Hello Bitch!------------- ")
    return render_template('home.jade', link=url_for('home.whatisthis', nomatter='priehali'))

@mod.route("/show/<nomatter>")
def whatisthis(nomatter):
    print("------------Got "+nomatter+"------------- ")
    return render_template('home.jade', msg=str(nomatter))

@mod.route("/add-to-db")
def dosomething():
    print("------------ ZAGLUSHKA activated------------- ")
    axa = AtFirst()
    axa.name = 'simple'
    db.session.add(axa)
    db.session.commit()

    return render_template('zaglushka.jade')
