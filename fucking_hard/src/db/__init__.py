from src import app
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy(app)

from src.db import models
