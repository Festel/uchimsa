from flask import Flask, g

app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('config.py')
app.jinja_env.add_extension('pyjade.ext.jinja.PyJadeExtension')
app.jinja_env.add_extension('jinja2.ext.loopcontrols')

from src.views import home

app.register_blueprint(home.mod)
