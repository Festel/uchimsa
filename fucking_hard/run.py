#!/usr/bin/python3
from src import app
from src.db import db
from flask_script import Manager, Command, Shell
from flask_migrate import Migrate, MigrateCommand

migrate = Migrate(app, db)

def run():
    app.run(
    debug=app.config['DEBUG'],
    host=app.config['HOST'],
    port=app.config['PORT'],
    )


manager = Manager(app, with_default_commands=False, usage= 'uchimsa commands')
manager.add_command('db', MigrateCommand)
manager.add_command("run", Command(run))
manager.add_command("shell", Shell())

if __name__ == '__main__':
    manager.run()
