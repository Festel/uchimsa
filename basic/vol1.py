# -*- coding: utf-8 -*-

# a = 77
# if a % 2 == 0:
#     print u'true'
#     print a**2
# else:
#     print u'false'
#     print a**3
# print 'END'


# a = 11
# if a == 11:
#     print '11'
# elif a == 22:
#     print '22'
# else:
#     print 'че нить другое'


# a = [1,4,77,-12,0]
# current = None
# while current != 0:
#     current = a.pop(0)
#     print current + 10
#     if current == 77:
#         break


# a = [1,4,77,-12,0]
# for x in a:
#     if x % 2 == 0:
#         print x


# def pepe(a, b):
#     i = b
#     res = 1
#     while i > 0:
#         res = res * a
#         i = i - 1
#     return res
#
# print pepe(2,5)


# кортеж - tuple()
# s = (12, 6)
# print s[0]


# словари - dict()
# d = {'alo':123, 'fuck':456}
# print d['alo']


# множества - set()
# x = {1,1,1,3,4}
# print x

# a = {1,2,3}
# b = {3,5,6}
# print a | b # операция объединения
# print a & b # операция пересечения (то что есть в обоих множествах)
# print a ^ b # операция вычитания (только уникальные значения)
