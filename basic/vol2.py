# -*- coding: utf-8 -*-

# def f (a,s,d):
#     print (a + ' и ' + s + ' но не ' + d)
# f('грека','река','хер')
# но можно еще и так
# args = ['рублик','бублик','хер']
# f(*args) #звездочка шоб показать шо передаем аргументы, а не параметр
# kwargs = {'d':'хер','a':'бублик','s':'лел'}
# f(**kwargs) #уже две звездочки шоб показать шо передача по названиям параметров


# анонимная функция
# lambda x,y,z: x + y if y > z else x + z
# map пропускает каждое значение списка через анонимную функцию
# print map(lambda x: x*2, [1,2,3])

# filter тут идет как готовый список, который прогоняется циклом, хотя по сути представляет из себя анонимную функцию перебирающую список alo
# alo = ['kek', 'pef', 123]
# for check in filter(lambda w: w != 123, alo):
#     print(check)

# почти тоже самое шо и выше, только вместо анонимной функции, тут идет цикл for с условием
# for check in (check for check in alo if not isinstance(check, int)):
#     print(check)

# чекает если хоть одно значение истина, то возвращает истину
# print any ([0,'',{},True])


# чекает если все значения истина, то возвращает истину
# print all ([0,'',{},True])


# def fib(max_number):
#     a,b = 1,1
#     while a < max_number:
#         yield a
#         a,b = b,a+b
#
# for x in fib(200):
#     print(x)


# bitch = input('sup m8, write number plz\n') #Считывает и возвращает строку входных данных из консольки
# if bitch.isdigit():
#     print(int(bitch)**2)
# else:
#     print('i said number asshole')


#для работы с файловой системой
# import os
# import shutil
# os.listdir('.') #обзор папопк
# os.getcwd() #отобразить текущий путь
# os.chdir() #сменить папку
# os.mkdir() #создать папку
# os.makedirs('adin/dva/tri') #создать определенный путь из папок, или записать внутрь существующей
# shutil.rmtree('adin') #удалить папку со вложенными файлами и папками
# os.path.exists('adin/dva') #чекает на наличие
# os.path.join('adin','dva') #создание кроссплатформенного пути (на unix например /, а на видусе \\)
# open('alo.txt','w') #функция создания файла
# shutil.move('alo.txt', 'adin') #функция переноса файла


#работа с сетью
# import urllib.request #класс для открытия и чтения урл ссылок
# r = urllib.request.urlopen('https://www.rambler.ru/') #запрос
# print(r.read()) #вывод содержимого

# import urllib.parse #класс для парсинга урл строк
# print(urllib.parse.urlencode({'page':'2', 'sort':'desc'})) #преобразовыввает передаваемы параметры в урл строку
