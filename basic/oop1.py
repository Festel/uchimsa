class Common: #объявление класса
    j = 12 #это типа свойства static
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def show(self):
        return self.a

class GetRekt(Common): #объявление класса, в скобках указывается наследуемый класс

    def __init__(self, a, b): #не совсем конструктор, скорее инициализация параметров класса
        super(self.__class__, self).__init__(a, b) #шо то типа пхпшного parent

    def __str__(self): #магический метод срабатываемый при выоводе объекта как строки
        return 'Chislo ' + str(self.pukan()) #а вот и утиная типизация подъехала, нужно число перевести строку чтобы вывести, инчае ошибка

    def pukan(self):
        return self.a + self.b

bitch = GetRekt(2, 5)
# print(bitch.a)
# print(bitch.pukan())
# print(bitch)
print(bitch.show())
