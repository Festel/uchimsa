import time

# описываем декоратор
def log(f):  # отображает имя вызываемой функции
    def decorated(*args, **kwargs):
        print('this function "' + f.__name__ + '"')
        res = f(*args, **kwargs)
        return res
    decorated.__name__ = f.__name__
    return decorated


def timeit(f):  # отображает время выполнения функции
    def decorated (*args, **kwargs):
        start = time.time()
        res = f(*args, **kwargs)
        print('Function '+ f.__name__ +' run in '+ str(time.time() - start))
        return res
    return decorated


def sleep(sec):  # не декоратор, а функция генерирующая декоратор, с переданным ей значением
    def decorator(f):  # а тут уже описываем декоратор
        def decorated (*args, **kwargs):
            time.sleep(sec)
            return f(*args, **kwargs)
        decorated.__name__ = f.__name__
        return decorated
    return decorator


# старый способ использования декоратора
# def summa(a, b):
#     return a + b
#
# kek = log(summa)
# print(kek(2, 3))


# новый способ использования декоратора
# @log
# def umnozh(a, b):
#     return a * b
#
# print(umnozh(2, 3))


# @timeit  # выполняется вторым
# @log  # выполняется первым
# def summa(a, b):
#     return a + b
#
# print(summa(4, 8))


@timeit
@sleep(2) # передавем значение функции, генерирующей декоратор
def summa(a, b):
    return a + b

print(summa(4, 8))
