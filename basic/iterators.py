class Reverse():
    def __init__(self, data):
        self.data = data
        self.index = len(data)

    def __iter__(self): #действие при попытке вызвать класс как итератор
        return self

    def __next__(self): #возврат элемента контейнера при проходе по нему
        self.index -= 1
        if self.index < 0:
            raise StopIteration
        return self.data[self.index]

    def __contains__(seld, x): #при проверке на принадлежность элемента контейнеру
        return x in self.data

kek = Reverse('fuck you')
for x in kek:
    print(x)
