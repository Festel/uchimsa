from django.db import models

class Post(models.Model):
    title = models.CharField('Title',  max_length=150)
    text = models.TextField('Text')
    create_date = models.DateTimeField('Creation date',  auto_now_add=True)

    def __str__(self): #для тайтла в списке постов в админке
        return self.title+' ('+str(self.create_date)+')';
