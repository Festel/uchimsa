from django.shortcuts import render
from .models import Post
from django.shortcuts import render_to_response

def post_list(request):
    post_list = Post.objects.all().order_by('-create_date')
    query = request.GET.get('query')
    if query:
        post_list = post_list.filter(text__icontains=query)
    return render_to_response('blog/post_list.html', {'post_list': post_list})

def post_detail(request, pk):
    post = Post.objects.get(id=pk)
    return render_to_response('blog/post_detail.html', {'post': post})
